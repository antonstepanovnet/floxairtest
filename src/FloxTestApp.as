package
{
	import net.antonstepanov.floxtest.FloxTestContext;
	import flash.display.Sprite;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	[SWF(backgroundColor="#FFFFFF", frameRate="60", width="300", height="400")]
	public class FloxTestApp extends Sprite
	{
		
		private var context:FloxTestContext;
		
		public function FloxTestApp()
		{
			context=new FloxTestContext(this);
		}
	}
}
