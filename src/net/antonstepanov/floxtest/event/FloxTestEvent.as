package net.antonstepanov.floxtest.event
{
	import flash.events.Event;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class FloxTestEvent extends Event
	{
		
		public static const SHOW_SCREEN:String="FloxTestEvent.SHOW_SCREEN";
		public static const SAVE_DATA:String="FloxTestEvent.SAVE_DATA";
		public static const SEND_LOG:String="FloxTestEvent.SEND_LOG";
		
		public static const REMOVE_CURRENT_SCREEN:String="FloxTestEvent.REMOVE_CURRENT_SCREEN";
		
		
		public var screenName:String;
		public var logEventName:String;
		public var logParams:Object;
		
		public function FloxTestEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
		}
		
		//
		// SOME SUGAR
		//
		public static function getShowScreen(screenName:String):FloxTestEvent {
			var showEvent:FloxTestEvent = new FloxTestEvent(FloxTestEvent.SHOW_SCREEN);
			showEvent.screenName=screenName;
			return showEvent;
		}
		
		public static function getSendLog(logStr:String,logParams:Object=null):FloxTestEvent {
			var showEvent:FloxTestEvent = new FloxTestEvent(FloxTestEvent.SEND_LOG);
			showEvent.logEventName=logStr;
			showEvent.logParams=logParams;
			return showEvent;
		}
		
	}
}
