package net.antonstepanov.floxtest.service
{
	import com.gamua.flox.Flox;

	import org.robotlegs.mvcs.Actor;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class TrackingService extends Actor
	{
		private const gameID:String="H69ZND1EUMvf26zJ"; 
		private const gameKey:String="w765glcUusVMiPfl";
		
		//TODO add namespace vars
		// CONFIG::FLOX_GAME_ID
		// CONFIG::FLOX_GAME_KEY
		
		public function TrackingService()
		{
			init();
		}

		
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//SETTERS AND GETTERS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//PUBLIC FUNCTIONS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		public function saveData():void {
			Flox.getTime(onServerTime, onServerTimeError);
		}
		
		
		public function sendLog(logStr:String,properties:Object=null):void {
//			trace("TrackingService.sendLog(",[logStr, JSON.stringify(properties)],")");
			Flox.logEvent(logStr,properties);
		}
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//PRIVATE FUNCTIONS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		private function init() : void
		{
			Flox.init(gameID, gameKey, "0.9");
		}
		
		private function saveEntitty(time : Date) : void
		{
			var entity:SampleEntity =new SampleEntity();
			entity.serverTime=time;
			entity.saveQueued();
		}
		
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//EVENT HANDLERS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		private function onServerTimeError(error:String, httpStatus:int) : void
		{
			trace("TrackingService.onServerTimeError(",[error, httpStatus],")");
		}
		private function onServerTime(time:Date) : void
		{
			trace("TrackingService.onServerTime(",[time],")");
			saveEntitty(time);
		}
	
		
	}
}
import com.gamua.flox.Entity;


class SampleEntity extends Entity
{
	public var serverTime : Date;
}

