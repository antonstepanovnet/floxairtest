package net.antonstepanov.floxtest.model
{
	import org.robotlegs.mvcs.Actor;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class TrackingModel extends Actor
	{
		
		private var eventCounterMap : Object={};
		//store events to log
		private var loggingEvents:Object={};
		
		//SET LOGGING
		
		public function setLogging(eventName : String):void {
			loggingEvents[eventName]={/*some loggins params*/}
		}
		
		/**
		 * simple logging validation
		 */
		public function needEventCounter(eventName : String):Boolean {
			return loggingEvents[eventName];
		}
		
		//
		//HANDLE EVENT COUNTERS
		//
		public function addEventCount(eventName : String) : void
		{
			if (!eventCounterMap[eventName]) {
				eventCounterMap[eventName]=1;
			}else {
				eventCounterMap[eventName]++;
			}
		}
		
		public function getEventCount(eventName:String):int {
			if (!eventCounterMap[eventName])
			{
				return 0;
			} else {
				return eventCounterMap[eventName];
			}
				
		}
		
		
		
	}
}
