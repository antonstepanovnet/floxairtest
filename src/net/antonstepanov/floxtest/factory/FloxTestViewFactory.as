package net.antonstepanov.floxtest.factory
{
	import net.antonstepanov.floxtest.static.Screen;
	import net.antonstepanov.floxtest.view.screens.base.BaseScreenView;
	import net.antonstepanov.floxtest.view.screens.intro.IntroScreenView;
	import net.antonstepanov.floxtest.view.screens.main.MainScreenView;
	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class FloxTestViewFactory
	{
		
		public static function getScreen(screenName:String):BaseScreenView {
			
			switch (screenName) {
				case Screen.INTRO:
					return new IntroScreenView();//Screen.INTRO
				case Screen.MAIN:
					return new MainScreenView();//Screen.MAIN
				
			}
			
			return null;
		}
		
	}
}
