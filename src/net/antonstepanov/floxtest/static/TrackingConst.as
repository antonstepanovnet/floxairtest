package net.antonstepanov.floxtest.static
{
	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class TrackingConst
	{
		public static const GO_MAIN_CLICK:String="go_main_clicked";
		public static const GO_INTRO_CLICK:String="go_intro_clicked";
		
	}
}
