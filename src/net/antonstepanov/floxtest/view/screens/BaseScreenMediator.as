package net.antonstepanov.floxtest.view.screens
{
	import flash.events.Event;
	import net.antonstepanov.floxtest.event.FloxTestEvent;
	import net.antonstepanov.floxtest.view.screens.base.BaseScreenView;

	import org.robotlegs.mvcs.Mediator;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class BaseScreenMediator extends Mediator
	{
		
		[Inject]
		public var view:BaseScreenView;
		
		override public function onRegister() : void
		{
			addViewListener(ScreenViewEvent.GO_TO_SCREEN_CLICK, gotoScreenClickHandler);
			addViewListener(ScreenViewEvent.SAVE_DATA_CLICK, saveDataClickHandler);
			addViewListener(ScreenViewEvent.LOG_EVENT, logEventHandler);

			addContextListener(FloxTestEvent.REMOVE_CURRENT_SCREEN, removeCurrentScreenHandler);
		}

		override public function onRemove() : void
		{
			view.dispose();			
		}

		//
		//VIEW HANDLERS
		//
		private function logEventHandler(e:ScreenViewEvent) : void
		{
			dispatch(FloxTestEvent.getSendLog(e.logEventName));
		}

		private function gotoScreenClickHandler(e:ScreenViewEvent) : void
		{
			dispatch(FloxTestEvent.getShowScreen(e.screenName));
		}
		
		
		private function saveDataClickHandler(e:ScreenViewEvent) : void
		{
			dispatch(new FloxTestEvent(FloxTestEvent.SAVE_DATA));
		}
		
		private function removeCurrentScreenHandler(e:Event) : void
		{
			//TODO move screen instance handling logic to ScreenManager
			view.parent.removeChild(view);
		}
		
		
	}
}
