package net.antonstepanov.floxtest.view.screens.main
{
	import net.antonstepanov.floxtest.static.TrackingConst;
	import net.antonstepanov.floxtest.static.Screen;
	import net.antonstepanov.floxtest.view.screens.ScreenViewEvent;
	import net.antonstepanov.floxtest.view.screens.base.BaseScreenView;

	import com.bit101.components.PushButton;

	import flash.events.Event;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class MainScreenView extends BaseScreenView
	{
		public function MainScreenView()
		{
			super();
			init();
		}
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//SETTERS AND GETTERS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//PUBLIC FUNCTIONS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//PRIVATE FUNCTIONS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		private function init() : void
		{
			label="Main Screen";
			addButton("Go to Intro", gotoIntroClickHandler);
			addButton("Save Data", saveDataClickHandler);
			
			transitionIn();
		}

		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//EVENT HANDLERS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		
		private function gotoIntroClickHandler(e:Event) : void
		{
			dispatchEvent( ScreenViewEvent.getLogEvent(TrackingConst.GO_INTRO_CLICK));
			dispatchEvent( ScreenViewEvent.getGotoScreenEvent(Screen.INTRO));
			
		}
		
		private function saveDataClickHandler(e:Event) : void
		{
			dispatchEvent(new ScreenViewEvent(ScreenViewEvent.SAVE_DATA_CLICK));
		}
		
	}
}
