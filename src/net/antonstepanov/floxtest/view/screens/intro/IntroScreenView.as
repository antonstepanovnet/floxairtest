package net.antonstepanov.floxtest.view.screens.intro
{
	import net.antonstepanov.floxtest.static.TrackingConst;
	import net.antonstepanov.floxtest.static.Screen;
	import net.antonstepanov.floxtest.view.screens.ScreenViewEvent;
	import net.antonstepanov.floxtest.view.screens.base.BaseScreenView;

	import flash.events.Event;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class IntroScreenView extends BaseScreenView
	{
		public function IntroScreenView()
		{
			super();
			init();
		}
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//SETTERS AND GETTERS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//PUBLIC FUNCTIONS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//PRIVATE FUNCTIONS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		private function init() : void
		{
			label="Intro Screen";
			addButton("Go to Main", gotoMainClickHandler);
			
			transitionIn();
		}

		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//EVENT HANDLERS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		
		private function gotoMainClickHandler(e:Event) : void
		{
			dispatchEvent( ScreenViewEvent.getLogEvent(TrackingConst.GO_MAIN_CLICK));
			dispatchEvent( ScreenViewEvent.getGotoScreenEvent(Screen.MAIN));
		}
		
	}
}
