package net.antonstepanov.floxtest.view.screens
{
	import flash.events.Event;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class ScreenViewEvent extends Event
	{
		
		public static const LOG_EVENT:String="ScreenEvent.LOG_EVENT";
		
		public static const SAVE_DATA_CLICK:String="ScreenEvent.SAVE_DATA_CLICK";
		public static const GO_TO_SCREEN_CLICK:String="ScreenEvent.GO_TO_SCREEN_CLICK";
		
		public var screenName : String;
		public var logEventName : String;

		public function ScreenViewEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
		}
		
		
		//
		//EVENT CREATION SUGAR
		// hide event creation impl details,
		//TODO add static const validation
		//
		public static function getGotoScreenEvent(screenName:String):ScreenViewEvent {
			var gotoEvent:ScreenViewEvent = new ScreenViewEvent(ScreenViewEvent.GO_TO_SCREEN_CLICK);
			gotoEvent.screenName=screenName;
			return gotoEvent;
		}
		
		public static function getLogEvent(logEventName:String):ScreenViewEvent {
			var logEvent:ScreenViewEvent = new ScreenViewEvent(ScreenViewEvent.LOG_EVENT);
			logEvent.logEventName=logEventName;
			return logEvent;
		}
		
	}
}
