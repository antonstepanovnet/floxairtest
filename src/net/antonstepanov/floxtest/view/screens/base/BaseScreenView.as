package net.antonstepanov.floxtest.view.screens.base
{
	import com.bit101.components.Label;
	import com.bit101.components.PushButton;
	import com.bit101.components.VBox;
	import com.greensock.TweenMax;

	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class BaseScreenView extends Sprite
	{
		private var screenLabel : Label;
		private var box : VBox;
		
		private var buttons:Vector.<ButtonItem>;
		
		
		public function BaseScreenView()
		{
			init();
		}

		
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//SETTERS AND GETTERS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		public function set label(value:String):void {
			screenLabel.text=value;
		}
		
		public function get label():String {
			return screenLabel.text;
		}
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//PUBLIC FUNCTIONS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		public function dispose() : void
		{
			var n : int = buttons.length;
			for (var i : int = 0; i < n; i++)
			{
				buttons[i].instance.removeEventListener(MouseEvent.CLICK, buttons[i].handler);
				buttons[i].instance=null;
				buttons[i].handler=null;
			}
			buttons=null;
		}
		
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//PUBLIC FUNCTIONS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		protected function addButton(label:String,handler:Function):void {
			
			buttons.push(
								new ButtonItem(
														//PushButton dispatches MouseEvent.CLICK
														new PushButton(box, 0, 0, label, handler),handler)
														);
		}
		
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//PRIVATE FUNCTIONS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		private function init() : void
		{
			buttons =new Vector.<ButtonItem>();
			
			
			
			var w : int = 300;
			var h : int = 400;
			var color : uint =  Math.random()*0xFFFFFF;
			with(this)
			{
				graphics.clear();
				graphics.beginFill(color, .5);
				graphics.drawRect(0, 0, w, h);
				graphics.endFill();
			}
			
			box =new VBox(this,20,20);
//			box.width=w;
//			box.height=h;
//			box.alignment=VBox.CENTER;
			box.spacing=10;
			screenLabel = new Label(box, 0, 0, "");
		}

		protected function transitionIn() : void
		{
			TweenMax.allFrom(getChildren(this), .3, {y:"-5",autoAlpha:0},.15);
		}
		public function getChildren(container:DisplayObjectContainer):Array
		{
			var ret:Array = [];
	
			var numChildren:int = container.numChildren;
			for (var i:int = 0; i < numChildren; ++i)
			{
				ret.push(container.getChildAt(i));
			}
	
			return ret;
		}
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//EVENT HANDLERS
		//::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
	}
}
import com.bit101.components.PushButton;

class ButtonItem {
	
	public var instance : PushButton;
	public var handler : Function;

	public function ButtonItem(instance:PushButton,handler:Function)
	{
		this.handler = handler;
		this.instance = instance;
	}
}

