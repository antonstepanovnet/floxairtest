package net.antonstepanov.floxtest.command
{
	import net.antonstepanov.floxtest.service.TrackingService;
	import net.antonstepanov.floxtest.model.TrackingModel;
	import net.antonstepanov.floxtest.event.FloxTestEvent;
	import org.robotlegs.mvcs.Command;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class SendLogCommand extends Command
	{
		
		[Inject]
		public var event:FloxTestEvent;
		
		[Inject]
		public var trackingModel:TrackingModel;
		
		[Inject]
		public var trackingService:TrackingService;
		
		override public function execute() : void
		{
			if (trackingModel.needEventCounter(event.logEventName))
			{
				trackingModel.addEventCount(event.logEventName);

				var eventLogProperties : Object = event.logParams ? event.logParams : {};
				eventLogProperties.amount = trackingModel.getEventCount(event.logEventName);

				trackingService.sendLog(event.logEventName, eventLogProperties);
			}
			else
			{
				trackingService.sendLog(event.logEventName);
			}
		}
	}
}
