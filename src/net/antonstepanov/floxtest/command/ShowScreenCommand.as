package net.antonstepanov.floxtest.command
{
	import net.antonstepanov.floxtest.event.FloxTestEvent;
	import net.antonstepanov.floxtest.factory.FloxTestViewFactory;
	import net.antonstepanov.floxtest.view.screens.base.BaseScreenView;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class ShowScreenCommand extends Command
	{
		
		[Inject]
		public var event:FloxTestEvent;
		
		override public function execute() : void
		{
			dispatch(new FloxTestEvent(FloxTestEvent.REMOVE_CURRENT_SCREEN));
			
			var screenView:BaseScreenView=FloxTestViewFactory.getScreen(event.screenName);
			contextView.addChild(screenView);
		}
	}
}
