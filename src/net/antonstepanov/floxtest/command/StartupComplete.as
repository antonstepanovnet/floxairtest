package net.antonstepanov.floxtest.command
{
	import net.antonstepanov.floxtest.static.TrackingConst;
	import net.antonstepanov.floxtest.model.TrackingModel;
	import net.antonstepanov.floxtest.event.FloxTestEvent;
	import net.antonstepanov.floxtest.static.Screen;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class StartupComplete extends Command
	{
		
		[Inject]
		public var trackingModel:TrackingModel;
		
		override public function execute() : void
		{
			//add loggging events
			trackingModel.setLogging(TrackingConst.GO_MAIN_CLICK);
			
			//open intro screen
			dispatch(FloxTestEvent.getShowScreen(Screen.INTRO));
			
		}
	}
}
