package net.antonstepanov.floxtest.command
{
	import net.antonstepanov.floxtest.event.FloxTestEvent;
	import net.antonstepanov.floxtest.service.TrackingService;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class SaveDataCommand extends Command
	{
		[Inject]
		public var event:FloxTestEvent;
		
		[Inject]
		public var trackingService:TrackingService;
		
		override public function execute() : void
		{
//			trace("SaveDataCommand.execute(",[],")");
			trackingService.saveData();

		}
	}
}
