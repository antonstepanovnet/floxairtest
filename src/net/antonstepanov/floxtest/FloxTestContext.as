package net.antonstepanov.floxtest
{
	import net.antonstepanov.floxtest.command.SendLogCommand;
	import net.antonstepanov.floxtest.command.SaveDataCommand;
	import net.antonstepanov.floxtest.service.TrackingService;
	import net.antonstepanov.floxtest.model.TrackingModel;
	import net.antonstepanov.floxtest.command.ShowScreenCommand;
	import net.antonstepanov.floxtest.command.StartupComplete;
	import net.antonstepanov.floxtest.event.FloxTestEvent;
	import net.antonstepanov.floxtest.view.screens.BaseScreenMediator;
	import net.antonstepanov.floxtest.view.screens.base.BaseScreenView;
	import net.antonstepanov.floxtest.view.screens.intro.IntroScreenView;
	import net.antonstepanov.floxtest.view.screens.main.MainScreenView;

	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.mvcs.Context;

	import flash.display.DisplayObjectContainer;

	/**
	 * @author antonstepanov
	 * @creation date Sep 17, 2015
	 */
	public class FloxTestContext extends Context
	{
		public function FloxTestContext(contextView : DisplayObjectContainer = null, autoStartup : Boolean = true)
		{
			super(contextView, autoStartup);
		}

		override public function startup() : void
		{
			//STARTUP COMMANDS
			commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, StartupComplete);
			
			//CONTEXT EVENTS HANDLERS
			commandMap.mapEvent(FloxTestEvent.SHOW_SCREEN, ShowScreenCommand);
			commandMap.mapEvent(FloxTestEvent.SAVE_DATA, SaveDataCommand);
			commandMap.mapEvent(FloxTestEvent.SEND_LOG, SendLogCommand);
			
			
			//SERVICES
			injector.mapSingleton(TrackingModel);
			injector.mapSingleton(TrackingService);
			
			//MEDIATORS
			mediatorMap.mapView(IntroScreenView, BaseScreenMediator,BaseScreenView);
			mediatorMap.mapView(MainScreenView, BaseScreenMediator,BaseScreenView);
			
			super.startup();
		}
	}
}
